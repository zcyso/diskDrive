module.exports = {
  runtimeCompiler: true,
  baseUrl: undefined,
  outputDir: undefined,
  assetsDir: 'static',
  productionSourceMap: undefined,
  parallel: undefined,
  css: undefined
}