import Vue from 'vue'
import Router from 'vue-router'

// 以下写法是懒加载写法，结合 Vue 的 异步组件 和 Webpack 的 code splitting feature, 轻松实现路由组件的懒加载。
const Layout = () => import('./components/layout/layout.vue');
const Dir = () => import('./views/Disk/layout.vue');
const Trash = () => import('./views/Trash/layout.vue');

Vue.use(Router);
export default new Router({
    mode: 'hash',
    base: process.env.BASE_URL,
    routes: [
        {
            path: '/',
            name: 'index',
            redirect: '/disk',
            meta: { model: '/index' },
        },
        {
            path: '/disk',
            name: 'disk_index',
            component: Layout,
            meta: { model: 'disk', parent: '/disk', id: 'disk_index', name:'disk'},
            children: [{
                path: 'dir/:dir_id?',
                alias: '/disk',
                component: Dir,
                meta: { model: 'disk', parent: '/disk', id: 'disk_index', name:'dir'}
            },{
                path: 'trash',
                component: Trash,
                meta: { model: 'disk', parent: '/disk', id: 'disk_index', name:'dir'}
            }]
        },
    ]
})
