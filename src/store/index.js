import Vue from 'vue'
import Vuex from 'vuex'
import store from './store'
import Actions from './actions'
import Mutations from './mutations'

Vue.use(Vuex)

export default new Vuex.Store({
  state: store,
  mutations: Mutations,
  actions: Actions
})
