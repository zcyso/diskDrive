import Disk from './modules/disk'
import Batch from './modules/batch'
import Uploader from './modules/uploader'
import User from './modules/user'

const Store = {
    disk: Disk,
    Batch: Batch,
    Uploader: Uploader,
    User: User
}

export default Store