let Env = 'production'; // development   production
let DevUrl = 'disk.zcyso.cc'; //测试服地址
let PRODUrl = 'disk.zcyso.cc'; //正式服地址
let DoUrl = Env == 'production' ? PRODUrl : DevUrl;
let PROD = Env == 'production' ? 'http://' : 'http://';
let ApiUrl = PROD + DoUrl;
let config = {
    NODE_ENV: Env,
    dourl: DoUrl,
    apiurl: ApiUrl,
    PROD_URL: PRODUrl,
    DEV_URL: DevUrl,
    system_info: {
        site_name:"CrazyPanda",
        status:1,
    }
}
export default config
