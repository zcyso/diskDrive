import {Message, Modal} from 'ant-design-vue'
import Store from '../store'
import config from './config'

const NODE_ENV = config.NODE_ENV
const DEV_URL = config.DEV_URL
const PROD_URL = config.PROD_URL

/**
 * 存储localStorage
 * @param name
 * @param content
 * @param duration Storage有效时间，单位：小时
 * @param set_time 是否设置时间
 * @returns {boolean}
 */
export const setStore = (name, content,set_time = false, duration = 0) => {
    if (!name) return false
    if (typeof content !== 'string') {
        content = JSON.stringify(content)
    }
    if (set_time) {
        let date = new Date
        if (duration > 0) {
            content += '&' + (date.getTime() + duration * 3600 * 1e3)
        } else {
            content += '&0'
        }
        content += '&' + (date.getTime())
    }
    window.localStorage.setItem(name, content)
}


/**
 * 获取localStorage
 * @param name
 * @param parse // 是否json格式化
 * @returns {boolean}
 */
export const getStore = (name, parse = false) => {
    if (!name) return false
    if (parse) {
        return JSON.parse(window.localStorage.getItem(name))
    }
    return window.localStorage.getItem(name)
}

/**
 * 删除localStorage
 */
export const removeStore = name => {
    if (!name) return false
    window.localStorage.removeItem(name)
}

/**
 * 添加cookie
 * @param name cookie名称
 * @param value cookie值
 * @param duration cookie有效时间，单位：小时
 */
export const addCookie = (name, value, duration) => {
    let n = name + '=' + escape(value) + '; path=/'
    if (duration > 0) {
        let date = new Date
        date.setTime(date.getTime() + duration * 3600 * 1e3)
        n = n + ';expires=' + date.toGMTString()
    }
    document.cookie = n
}

/**
 * 获取cookie
 * @param name cookie名称
 * @returns {null}
 */
export const getCookie = (name) => {
    let t = document.cookie
    let a = t.split('; ')
    for (let n = 0; n < a.length; n++) {
        let r = a[n].split('=')
        if (r[0] === name) {
            return unescape(r[1])
        }
    }
    return null
}

/**
 * 移除cookie
 * @param name cookie名称
 */
export const delCookie = (name) => {
    let t = new Date
    t.setTime(t.getTime() - 1)
    let a = getCookie(name)
    if (a !== null) document.cookie = name + '=' + a + '; path=/;expires=' + t.toGMTString()
}

export const format_date = (data, show = true) => {
    //格式化时间
    let now = new Date(data * 1000);
    let year = now.getFullYear();
    let month = now.getMonth() + 1;
    let date = now.getDate();
    let hour = now.getHours();
    let minute = now.getMinutes();
    // let second = now.getSeconds();
    if (month < 10) {
        month = '0' + month;
    }
    if (date < 10) {
        date = '0' + date;
    }
    if (hour < 10) {
        hour = '0' + hour;
    }
    if (minute < 10) {
        minute = '0' + minute;
    }
    const finally_date = {
        year: year,
        month: month,
        day: date,
        hour: hour,
        minute: minute
    }
    if (show) {
        return year + "-" + month + "-" + date + "   " + hour + ":" + minute;
    } else {
        return finally_date
    }
}
/**
 * 转换时间为可阅读格式，传入date的time值
 * @param time
 * @returns {*}
 */
export const prettyTime2Chinese = (time) => {
    if (!time) {
        return '';
    }
    if (isNaN(time)) {
        return '格式不正确';
    }
    var minute = 60 * 1000, //1分钟
        hour = 60 * minute, //1小时
        day = 24 * hour, //1天
        month = 12 * day,//月
        year = 12 * month;//年

    var diff = new Date().getTime() - time;
    var r = 0;
    if (diff > year) {
        r = parseInt(diff / year);
        return r + "年前";
    }
    if (diff > month) {
        r = parseInt(diff / month);
        return r + "个月前";
    }
    if (diff > day) {
        r = parseInt(diff / day);
        if (r == 1) {
            return "昨天";
        }
        return r + "天前";
    }
    if (diff > hour) {
        r = parseInt(diff / hour);
        return r + "个小时前";
    }
    if (diff > minute) {
        r = parseInt(diff / minute);
        return r + "分钟前";
    }
    return "刚刚";
};


export const format_date_now = (day) => {
    //格式化时间
    if (day == undefined) {
        day = 0;
    }
    let now = new Date();
    let year = now.getFullYear();
    let month = now.getMonth();
    let date = now.getDate() + day;
    if (month < 10) {
        month = '0' + month;
    }
    if (date < 10) {
        date = '0' + date;
    }
    return year + "-" + month + "-" + date;
}

export const date2str = (date, fmt) => {
    if (/(y+)/.test(fmt)) {
        fmt = fmt.replace(RegExp.$1, (date.getFullYear() + '').substr(4 - RegExp.$1.length));
    }
    let o = {
        'M+': date.getMonth() + 1,
        'd+': date.getDate(),
        'h+': date.getHours(),
        'm+': date.getMinutes(),
        's+': date.getSeconds()
    };
    for (let k in o) {
        if (new RegExp(`(${k})`).test(fmt)) {
            let str = o[k] + '';
            fmt = fmt.replace(RegExp.$1, (RegExp.$1.length === 1) ? str : padLeftZero(str));
        }
    }
    return fmt;
}
function padLeftZero(str) {
    return ('00' + str).substr(str.length);
}

/**
 * 数字转英文字母
 * 如：1->A
 * @param num
 * @returns {string}
 */
export const convert = (num) => {
    let result = "";
    while (num) {
        result = String.fromCharCode(--num % 26 + 65) + result;
        num = Math.floor(num / 26)
    }
    return result
}
// 判断参数是否是其中之一
export function oneOf (value, validList) {
    for (let i = 0; i < validList.length; i++) {
        if (value === validList[i]) {
            return true;
        }
    }
    return false;
}

/**
 * 判断某dom是否有滚动条
 * @param el dom对象
 * @param direction 垂直滚动或水平滚动
 * @returns {boolean}
 */
export const hasScrolled = (el, direction = "vertical") => {
    if(!el) {
        return false;
    }
    let overflow = el.currentStyle ? el.currentStyle.overflow :
        window.getComputedStyle(el).getPropertyValue("overflow");
    if (overflow === "hidden") return false;

    if (direction === "vertical") {
        return el.scrollHeight > el.clientHeight;
    } else if (direction === "horizontal") {
        return el.scrollWidth > el.clientWidth;
    }
}

/**
 * 多数组合并
 * @param arrList
 * @returns {Array}
 */
export const array_merge = (...arrList) => {
    let array = [];
    const middeleArray = arrList.reduce((a,b) => {
        return a.concat(b);
    });
    middeleArray.forEach((arrItem) => {
        if(array.indexOf(arrItem) == -1){
            array.push(arrItem);
        }
    });
    return array;
}


// 格式化文件大小
export const formatSize = (size) => {
    size = parseFloat(size)
    if(size == 1099511627776000000) {
        return '无限容量';
    } else if (size === 0) {
        return '0 bytes'
    } else if (size < 1024) {
        return size.toFixed(0) + ' bytes'
    } else if (size < 1024 * 1024) {
        return (size / 1024.0).toFixed(0) + ' KB'
    } else if (size < 1024 * 1024 * 1024) {
        return (size / 1024.0 / 1024.0).toFixed(1) + ' MB'
    } else if (size < 1024 * 1024 * 1024 * 1024){
        return (size / 1024.0 / 1024.0 / 1024.0).toFixed(1) + ' GB'
    } else if (size < 1024 * 1024 * 1024 * 1024 * 1024){
        return (size / 1024.0 / 1024.0 / 1024.0 / 1024.0).toFixed(1) + ' TB'
    } else {
        return (size / 1024.0 / 1024.0 / 1024.0 / 1024.0 / 1024.0).toFixed(1) + ' PB'
    }
}

// 获取没有上传的文件
export const getNotUploadFile = (fileList, max_len) => {
    let ActLen = 0;
    let ActFile = [];
    if(fileList.length < 1) {
        return {
            ActLen: ActLen,
            ActFile: ActFile,
        }
    }
    for (let file of fileList) {
        if(file.isFolder == true) {
            let info = getNotUploadFile(file.fileList, max_len);
            ActLen += info.ActLen;
            ActFile = array_merge(info.ActFile,ActFile);
        } else {
            if(file.isrun == true) {
                ActLen++;
            } else if(file.is_task == true && file.run_type != 'success') {
                if(ActFile.length < max_len) {
                    file.isrun = true;
                    ActFile.push(file);
                }
            }
        }
    }
    return {
        ActLen: ActLen,
        ActFile: ActFile,
    }
}

export const blobSlice = File.prototype.slice || File.prototype.mozSlice || File.prototype.webkitSlice;