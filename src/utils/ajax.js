import axios from 'axios'
import config from './config'
import { notification } from 'ant-design-vue'

const ajaxUrl = config.apiurl

const $ajax = {
    get: function (url, data, config) {
        return _method(url, data, config, 'get')
    },
    post: function (url, data, config) {
        return _method(url, data, config, 'post')
    },
    delete: function (url, data, config) {
        return _method(url, data, config, 'delete')
    },
    head: function (url, data, config) {
        return _method(url, data, config, 'head')
    },
    put: function (url, data, config) {
        return _method(url, data, config, 'put')
    },
    patch: function (url, data, config) {
        return _method(url, data, config, 'patch')
    }
}
const _method = function (url, data, config, method) {
    var qs = require('qs')

        let AjaxConfig = {
            method: method,
            url: ajaxUrl + url,
            timeout: 5000,
            data: method == 'delete' ? data : qs.stringify(data),
            responseType: config ? config.responseType : 'json',
            headers: {Authorization: window.localStorage.getItem('token')}
        };
        if(method == 'get'){
            AjaxConfig.params = data;
        }

        return axios(AjaxConfig).then(function (ret) {
            return ret.data;
        }.bind(this)).catch(function () {
            notification.error({
                message: '请求出错！',
                description: '系统出现错误，请刷新页面并且联系管理员.',
            });
        }.bind(this));


    switch (method) {
        case 'get':
            return axios.get(ajaxUrl + url, {params:data});
            break
        case 'post':
            return axios.post(ajaxUrl + url, {data:data})
            break
        case 'delete':
            return axios.delete(ajaxUrl + url, {data:data})
            break
        case 'head':
            return axios.head(ajaxUrl + url, {params:data})
            break
        case 'put':
            return axios.put(ajaxUrl + url, {params:data})
            break
        case 'patch':
            return axios.patch(ajaxUrl + url, config)
            break
        default:
            return axios.get(ajaxUrl + url, config)
            break
    }
}

export default $ajax
