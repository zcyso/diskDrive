import Vue from 'vue'
import Vuex from 'vuex'
import { getStore } from './utils'

Vue.use(Vuex)
const state = {
    pageLoding: false,
    editItemNameData: {}
}
export default new Vuex.Store(state)
