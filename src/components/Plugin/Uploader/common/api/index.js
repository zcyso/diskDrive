import Ajax from '../../../../../utils/ajax'
import ApiUrl from '../../../../../config/api'

export function testUploadFie(data) {
    return Ajax.get(ApiUrl.UploadApiUrl.uploadTest,data);
}

export function mergeFie(data) {
    return Ajax.post(ApiUrl.UploadApiUrl.merge,data);
}