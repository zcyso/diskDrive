import DiskApiUrl from './disk'
import FileApiUrl from './file'
import Batch from './batch'
import Upload from './upload'

const ApiUrl = {
    BatchApiUrl: Batch,
    DiskApiUrl: DiskApiUrl,
    FileApiUrl: FileApiUrl,
    UploadApiUrl: Upload,
}
export default ApiUrl