const DiskApiUrl = {
    BatchList: '/batch/batchList',
    BatchTrash: '/batch/batchTrash',
    Delete: '/batch/delete',
    ReName: '/batch/rename',
    Zone: '/member/zone',
}
export default DiskApiUrl