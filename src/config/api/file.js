import config from '../../utils/config'

const FileApiUrl = {
    ReName: '/disk/v1/rename',
    Download: config.apiurl+'/d',
}
export default FileApiUrl