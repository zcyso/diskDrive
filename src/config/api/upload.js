import config from '../../utils/config'

const UploadApiUrl = {
    upload: config.apiurl+'/upload',
    uploadTest: '/upload/checkfile',
    getConfig:'/upload/conf',
    merge:'/upload/merge',
    check_zone:'/upload/zone'
}
export default UploadApiUrl