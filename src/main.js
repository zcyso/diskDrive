import Vue from 'vue'
import App from './App.vue'
import store from './store/'
import router from './router'
import Antd from 'ant-design-vue'
import VueContextMenu  from 'vue-context-menu'
import 'ant-design-vue/dist/antd.css'
import $ajax from './utils/ajax'
import config from './utils/config'
import utils from './utils/utils'
import './assets/css/app.less'

Vue.prototype.$config = config;
Vue.prototype.$utils = utils;
Vue.prototype.$ajax = $ajax;
Vue.prototype.Bus = store.state;

Vue.config.productionTip = false;

Vue.use(Antd);
Vue.use(VueContextMenu);

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app');
